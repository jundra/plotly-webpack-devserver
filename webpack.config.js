var webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: "./index.js",
    devServer: {
        contentBase: '.',
    },
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    'ify-loader',
                    'transform-loader?plotly.js/tasks/compress_attributes.js',
                ]
            },
        ]
    }
};
